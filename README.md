# hackathonEsferico

El espiruto de este taller es trabajar en como ecribimos codigo y como lo leemos. Ademas de que practicas usamos para hacer que el mismo es mas sencillo de interpretar y modificar por el programador del futuro.

## Dinamica:

En equipos de hasta 3 personas tienen que tomar unos de los cuatros proyectos a continuacion, crear una rama y ponerse a trabajar en la solucion que uds crean mejor, es decir que pueden elegir la tecnologia, el framework, hacer o no test, etc. En algun momento, se les va a pedir que detengan el desarrollo y que cambien de proyecto en el cual tendran que hacer una pequeña modificacion. 

### Normas: 

1 Todos los equipos tienen que tener un nombre.
2 Las ramas tienen que ser nombradas con la siguiente nomenclatura nombreequipo-nombreProyecto.

## Proyecto:

### La granja de zenon

Se nos pide crear un sistema para cosechadoras que recorra un campo y evalue si tiene que cosechar o no los cultivos del mismo. El campo es un array de nxn que en cada celda se puede encontrar tres tipos distintos de cultivos trigo, maiz y girasoles. Cada uno de esto tiene un tiempo distinto de maduracion y solo tienen que se recolectados si los mismo estan en el momento justo.

Tiempos de maduracion.
Trigo 10 dias desde que se planto
Maiz 15 dias desde que se planto
Girasoles 20 dias desde que se planto

Nota el campo se usa la tenica de rotacion, asi que en una mismas matris puede estar los tres tipo de cultivos juntos y la cosechadora tiene que ser capas de poder recolectar a todos de ser necesario.

Extra: Se agrego un nuevo tipo de cultivo que son tomates, maduran en 5 dias.

### ACAS 

Nos contrata una empresa para desarrollar un sistema que evite que dos aviones que viajan a la mismas altura coliconen. El sistema tiene que ser capas de detectar cuando dos aviones estan a la mismas altura y cuando esto ocurra le indicara a uno que suba y al otro que decienda. 

Los aviones tiene velocidad, altitud, altitud maxima y modelo. 

En caso de que un avion no pueda acender porque esta en su altitud operativa limite, el sitema tendra que indicarle a un avion que gira a la derecha y el otro a la izquierda.

Extra: 

### Mars Rover

La nasa nos a pedido diseñar el sistema operativo del rover que precedera al perserverans en marte. El mismo tiene que ser capas de desplzarse por el espacio con 3 comandos, avanzar, girarIzquierda, girarDerecha.

Avanzar toma como parametros la cantidad de metros y 1 o -1, 1 si se quiere ir para adelante y -1 si se quiere ir para atras
Los comandos de girar se difinio que siempre seran en 90 o -90 grados. Asi que con solo llamarlos se asume que girar quedando perpendicular a la linea anterior a donde estaba mirando antes.

por ultimo el rover contendra, una direccion a donde esta mirando y un punto inicial que es donde se encuentra, por lo que si se le pregunta la posicion el mismo tendria que ser capas de retornar donde se encuentra parado.

### Evaluador de calves

Se nos pide programar un modulo que evalue que tan buenas son las contraseñas de los usarios de un sistema, la escala a utilizar sera la seriade fibonacci (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55) siendo 0 muy malo y 55 excelente. 

Si la contrase tiene solo letras o solo numeros, sumara 1
Si tiene mayuscualas y minusculas es sumara 1 
Si tiene letras con numeros sumara 5
Si tiene letas con nuemros y mayuscualas con minusculas sumara 8
Si tiene menos de 8 caracteres sumara 0 
Si tiene entre 8 a 16 sumara 3
Si tiene mas de 16 suamara 8
Si tiene simbolos sumara 8

extra: si las contraseñas estan dentro de esta lista [admin,1234,contraseña,Admin,Nimda,123456,4321] resta 21 puntos.